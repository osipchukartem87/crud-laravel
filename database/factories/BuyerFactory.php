<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Buyer;
use Faker\Generator as Faker;

$factory->define(Buyer::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'country' => $faker->country,
        'city' => $faker->numberBetween(1, 100),
        'addressLine' => $faker->address,
        'phone' => $faker->phoneNumber
    ];
});
