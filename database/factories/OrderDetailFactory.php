<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product_id' => $faker->numberBetween(1, 200),
        'quantity' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(500, 50000),
        'discount' => $faker->numberBetween(1, 100),
    ];
});
