<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Order::class, 10)->create()
            ->each(function ($order){
                $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, 5)
                        ->make()
                );
            });
        factory(\App\Buyer::class, 10)->create()
            ->each(function ($buyer){
                $buyer->orders()->saveMany(
                    factory(\App\Order::class, 3)
                        ->make()
                );
            });
    }
}
