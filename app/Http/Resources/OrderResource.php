<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId'=> $this->order_id,
            'orderDate' =>  $this->date,
            'orderSum' =>  $this->sum,
            'orderItems' =>  [
                'productName' =>  $this->name,
                    'productQty' =>  $this->quantity,
                    'productPrice' =>  $this->price,
                    'productDiscount' =>  $this->discount,
                    'productSum' => $this->price * $this->discount * $this->quantity
                ]
        ];
                    /*buyer: {
                                buyerFullName: name+surname,
                        buyerAddress: country+city+addressLine,
                        buyerPhone: phone
                    }*/

    }
}
