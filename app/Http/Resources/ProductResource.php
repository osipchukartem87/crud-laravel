<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price/100,
            'seller' => $this->seller->name,
            'available' => !!$this->available
        ];
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if (!$product) {
            return new Response([
                'result' => 'fail',
                'message' => 'product not found'
            ]);
        }

        $data = Input::all();

        foreach ($data as $key => $value) {
            if ($key === 'price') {
                $product->$key = $value*100;
            } else {
                $product->$key = $value;
            }
        }
        $product->save();

        return new Response(new ProductResource($product));
    }
}
