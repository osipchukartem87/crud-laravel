<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        $order = Order::create($data);

        $order->available = $data['available'];
        $order->save();

        return new Response($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id)->where();

        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        $data = Input::all();

        foreach ($data as $key => $value) {
            if ($key === 'price') {
                $order->$key = $value*100;
            } else {
                $order->$key = $value;
            }
        }
        $order->save();

        return new Response(new ProductResource($order));
    }
}
